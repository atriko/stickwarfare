﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleAttacking : MonoBehaviour
{
    [SerializeField]
    private Joystick joystick;
    private Transform myTransform;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        RotateByJoystick();
    }

    void RotateByJoystick()
    {
        myTransform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(joystick.Vertical, joystick.Horizontal) * 180 / Mathf.PI);

    }
}
