﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleMovement : MonoBehaviour
{
    [SerializeField]
    private Joystick joystick;
    private Rigidbody2D myRigidbody;
    private float movementSpeed = 4f;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (joystick.Horizontal > .8f || joystick.Horizontal < .8f || joystick.Vertical > .8f || joystick.Vertical < .8f)
        {
            Move(joystick.Horizontal, joystick.Vertical);
        }
        else
        {
            myRigidbody.velocity = Vector2.zero;
        }
    }

    void Move(float horizontal,float vertical)
    {
        myRigidbody.velocity = new Vector2(horizontal * movementSpeed, vertical* movementSpeed);
    }
}
